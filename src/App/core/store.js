import Store from 'electron-store'
import defaults from './default-config'

const store = new Store({ defaults })

export default store
