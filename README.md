# Form Funnel

> A simple, specialized electron app for quickly filling out common Form-fillable PDFs.

Built for a specific use-case, this form filler saves time when you've got groups of forms that are commonly used with different data.

As an example: for each house built in a township, someone at a construction company typically fills out a bunch of forms for various utilities, HOAs, certification forms, internal documents, etc. With this app, you can just put that data in a Google Sheet, connect this app with that sheet, select the PDFs and press `Fill 'em` to complete the forms.

This app uses a column's label to find the field in the PDF to fill.

<img>

<hr>
Functionality:

- Link a Google Sheets document
- Select and save form-fillable PDFs (FDFs)
- Fill those PDFs with selected rows from the Google Sheet
- Locally remember all selections so that future use is much faster

There you have it!

I collect crash and usage information so that I can find bugs and improve the UX.

<hr>
## Future updates . . .

- Refined UX
- Ability to import data from files
- Load and fill PDFs from many sheets / datasets
- Allow grouping PDFs by column entry to a sheet
  - This would enable using an index sheet to control multiple form-group fillings
